<?php namespace App\Models;
use CodeIgniter\Model;

class RoomModel extends Model
{

    protected $table = 'room'; //таблица, связанная с моделью
    protected $allowedFields = ['id_housing', 'room_number', 'number_seats', 'price', 'picture_url', 'user_id'];

    public function getRoom($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getRooms($user_id = null, $search = '', $per_page = null)
    {

        $model = $this->select('*');
        if (!is_null($user_id)) {
            $model = $model->where('user_id', $user_id);
        }
        // Пагинация
        return $model->paginate($per_page, 'group1');
    }

    public function getRoomWithUser($id = null, $search = ' ')
    {
        $builder = $this->select('room.id, room.picture_url, room_number, number_seats, price, email')
        ->join('users', 'user_id = users.id');
        if ($search)
            $builder->where('room_number', (int) $search)->orWhere('number_seats', (int) $search);

        if (!is_null($id))
        {
            return $builder->where(['room.id' => $id])->first();
        }
        return $builder;
    }
}