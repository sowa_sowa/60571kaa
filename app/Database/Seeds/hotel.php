<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class hotel extends Seeder
{
    public function run()
    {
        $data = [
            'name' => 'Снегирь',
        ];
        $this->db->table('housing')->insert($data);
        $data = [
            'name' => 'Лондон',
        ];
        $this->db->table('housing')->insert($data);

        $data = [
            'id_housing'=> 1,
            'room_number' => 100,
            'number_seats' => 2,
            'price' => 5000,
        ];
        $this->db->table('room')->insert($data);

        $data = [
            'id_housing'=> 2,
            'room_number' => 105,
            'number_seats' => 3,
            'price' => 5500,
        ];
        $this->db->table('room')->insert($data);

        $data = [
            'name'=> 'Петров Иван Иванович',
        ];
        $this->db->table('guest')->insert($data);
        $data = [
            'name'=> 'Иванов Алексей Викторович',
        ];
        $this->db->table('guest')->insert($data);
        $data = [
            'name'=> 'Иванов Алексей Алексеевич',
        ];
        $this->db->table('guest')->insert($data);
        $data = [
            'name'=> 'Иванов Алексей Дмитривич',
        ];
        $this->db->table('guest')->insert($data);

        $data = [
            'id_room'=> 1,
            'id_guest'=> 1,
            'start_date' => '2021-01-04',
            'end_date' => '2021-01-28',
            'number_people' => '2',
        ];
        $this->db->table('accommodation')->insert($data);
        $data = [
            'id_room'=> 2,
            'id_guest'=> 2,
            'start_date' => '2021-01-04',
            'end_date' => '2021-01-28',
            'number_people' => '2',
        ];
        $this->db->table('accommodation')->insert($data);
        $data = [
            'id_room'=> 1,
            'id_guest'=> 3,
            'start_date' => '2021-02-04',
            'end_date' => '2021-02-28',
            'number_people' => '2',
        ];
        $this->db->table('reservation')->insert($data);
        $data = [
            'id_room'=> 2,
            'id_guest'=> 4,
            'start_date' => '2021-02-04',
            'end_date' => '2021-02-28',
            'number_people' => '2',
        ];
        $this->db->table('reservation')->insert($data);


    }
}

