<?php

namespace App\Database\Migrations;
use CodeIgniter\Database\Migration;

class Hotel extends Migration
{
    public function up()
    {
        if (!$this->db->tableexists('housing'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('housing', TRUE);
        }

        if (!$this->db->tableexists('room'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_housing' => array('type' => 'INT', 'unsigned' => TRUE),
                'room_number' => array('type' => 'INT', 'null' => FALSE),
                'number_seats' => array('type' => 'INT', 'null' => FALSE),
                'price' => array('type' => 'INT', 'null' => FALSE),
                'user_id' => array('type' => 'int', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('user_id','users','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_housing','housing','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('room', TRUE);
        }

        if (!$this->db->tableexists('guest'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('guest', TRUE);
        }

        if (!$this->db->tableexists('accommodation'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_room' => array('type' => 'INT', 'unsigned' => TRUE),
                'id_guest' => array('type' => 'INT', 'unsigned' => TRUE),
                'start_date' => array('type' => 'DATE', 'null' => TRUE),
                'end_date' => array('type' => 'DATE', 'null' => TRUE),
                'number_people' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_room','room','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_guest','guest','id','RESTRICT','RESTRICT');

            // create table
            $this->forge->createtable('accommodation', TRUE);
        }

        if (!$this->db->tableexists('reservation'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_room' => array('type' => 'INT', 'unsigned' => TRUE),
                'id_guest' => array('type' => 'INT', 'unsigned' => TRUE),
                'start_date' => array('type' => 'DATE', 'null' => TRUE),
                'end_date' => array('type' => 'DATE', 'null' => TRUE),
                'number_people' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_room','room','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_guest','guest','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('reservation', TRUE);
        }
    }

    public function down()
    {
        $this->forge->droptable('accommodation');
        $this->forge->droptable('reservation');
        $this->forge->droptable('housing');
        $this->forge->droptable('guest');
        $this->forge->droptable('room');
    }
}
