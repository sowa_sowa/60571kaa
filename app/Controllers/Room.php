<?php namespace App\Controllers;
use App\Models\RoomModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Room extends BaseController
{
    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('room/create', $this->withIon($data));
    }
    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'room_number'   => 'required|numeric',
                'number_seats'  => 'required|numeric',
                'price'         => 'required|numeric',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new RoomModel();
            $data = [
                'id_housing' => 1,
                'room_number' => $this->request->getPost('room_number'),
                'number_seats' => $this->request->getPost('number_seats'),
                'price' => $this->request->getPost('price'),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Curating.room_create_success'));
            return redirect()->to('/room');
        }
        else
        {
            return redirect()->to('/room/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RoomModel();

        helper(['form']);
        $data ['room'] = $model->getRoom($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('room/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/room/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'            => 'required',
                'room_number'   => 'required|numeric',
                'number_seats'  => 'required|numeric',
                'price'         => 'required|numeric',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new RoomModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'id_housing' => 1,
                'room_number' => $this->request->getPost('room_number'),
                'number_seats' => $this->request->getPost('number_seats'),
                'price' => $this->request->getPost('price'),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);
            session()->setFlashdata('message', lang('Room.r\oom_update_success'));
            return redirect()->to('/room');
        }
        else
        {
            return redirect()->to('/room/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RoomModel();
        $model->delete($id);
        return redirect()->to('/room');
    }
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RoomModel();
        $data ['room'] = $model->getRoom();
        echo view('room/view_all', $this->withIon($data));
    }
    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }

        $model = new RoomModel();
        $data ['room'] = $model->getRoom($id);
        echo view('room/view', $this->withIon($data));
    }
    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin()) {

            ///Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new RoomModel();

            $data['room'] = $model->getRoomWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('room/view_all_with_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Room.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }
}
