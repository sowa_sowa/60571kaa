<?php

namespace App\Controllers;

use App\Models\OAuthModel;
use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use Aws\S3\S3Client;

class RoomApi extends ResourceController
{
    protected $modelName = 'App\Models\RoomModel';
    protected $format = 'json';
    protected $oauth;

    public function __construct()
    {
        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: *'); //method
    }

    public function delete($id = null)
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $this->model->delete($id);
            return $this->respondDeleted(null, 'Room deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }

    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $query = $model->db->table('users')
                ->select('*')
                ->where('username', $OAuthModel->getUser()->user_id)
                ->get();

            $id = ($query->getRow())->id;

            $insert = null;

            if ($this->request->getMethod() === 'post' && $this->validate([
                    'id_housing' => 'required',
                    'room_number' => 'required',
                    'number_seats' => 'required',
                    'price' => 'required',
                    'picture' => 'is_image[picture]|max_size[picture,1024]',
                ])) {

                //получение загруженного файла из HTTP-запроса
                $file = $this->request->getFile('picture');
                if ($file->getSize() != 0) {
                    //подключение хранилища
                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region' => 'us-east-1',
                        'endpoint' => getenv('S3_ENDPOINT'),
                        'use_path_style_endpoint' => true,
                        'credentials' => [
                            'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                            'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                        ],
                    ]);

                    //получение расширения имени загруженного файла
                    $ext = explode('.', $file->getName());
                    $ext = $ext[count($ext) - 1];
                    //загрузка файла в хранилище

                    $insert = $s3->putObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        //генерация уникального имени файла конкатенацией случайного числа и метки времени:
                        'Key' => getenv('S3_KEY') . '/file' . time() . rand(100000, 999999) . '.' . $ext,
                        'Body' => fopen($file->getRealPath(), 'r+')
                    ]);
                }

                //подготовка данных для модели
                $data = [
                    'id_housing' => $this->request->getPost('id_housing'),
                    'room_number' => $this->request->getPost('room_number'),
                    'number_seats' => $this->request->getPost('number_seats'),
                    'price' => $this->request->getPost('price'),
                    'user_id' => $id,
                ];

                //если изображение было загружено и была получена ссылка на него то добавить ссылку в данные модели
                if (!is_null($insert))
                    $data['picture_url'] = $insert['ObjectURL'];
                $model->save($data);

                return $this->respondCreated(null, 'Room created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();
    }

    public function room() //Отображение всех записей
    {

        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $query = $model->db->table('users')
                ->select('*')
            ->where('username', $OAuthModel->getUser()->user_id)
            ->get();

            $id = ($query->getRow())->id;
            $data =  $model->getRooms($id, $search, $per_page);
//            Ответ контроллера включает данные (room и параметры пагинации (pager)
            return $this->respond(['room' => $data, 'pager' => $model->pager->getDetails('group1')]);
        }
        else
        {
            return
            $this->oauth->server->getResponse()->send();
        }
    }

}