<?php

namespace App\Controllers;
use App\Models\OAuthModel;
use App\Services\GoogleClient;
use App\Services\OAuth;
use OAuth2\Request;

class OAuthController extends BaseController
{
    private GoogleClient $googleClient;
    private OAuthModel $OAuthModel;
    private OAuth $OAuth;

    public function __construct()
    {
        $this->OAuth = new OAuth();
        $this->OAuthModel = new OAuthModel();
        $this->googleClient = new GoogleClient('frontend');
        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: *'); //method
    }


    public function Authorize()
    {

        $request = Request::createFromGlobals();
        $this->OAuth->server->handleTokenRequest($request)->send();

    }

    public function user()
    {

        if ($this->OAuth->isLoggedIn()) {
            return json_encode($this->OAuthModel->getUser());
        }
        else return ("Ошибка получения пользователя");
    }


    public function AuthUrl(): string
    {

        return $this->googleClient->getGoogleClient()->createAuthUrl();

    }
}