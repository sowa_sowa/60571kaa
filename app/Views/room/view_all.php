<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main" >
        <h2>Все комнаты</h2>

        <?php if (!empty($room) && is_array($room)) : ?>

            <?php foreach ($room as $item): ?>

                <div class="card mb-3" style="max-width: 600px; background-color: #F7F4DEED">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) : ?>
                            <?php else:?>
                                <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['room_number']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Номер команты: <?= esc($item['room_number']); ?></h5>
                                <p class="card-text">Количество мест: <?= esc($item['number_seats']); ?></p>
                                <a href="<?= base_url()?>/index.php/room/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти рейтинги.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>