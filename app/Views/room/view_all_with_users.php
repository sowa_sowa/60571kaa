<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="" >
    <?php if (!empty($room) && is_array($room)) : ?>
        <h2>Все комнаты:</h2>
        <div class="d-flex justify-content-between ">
            <?= $pager->links('group1', 'my_page') ?>

            <?= form_open('room/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('room/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Номер комнаты или количество мест" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
            </form>
        </div>
        <table class="table table-striped" style="margin: 0; background-color: #F7F4DEED">
            <thead>
            <th scope="col">Фотография</th>
            <th scope="col">Номер комнаты</th>
            <th scope="col">Количество мест</th>
            <th scope="col">Цена</th>
            <th scope="col">Email</th>
            <th scope="col">Управление</th>
            </thead>
            <tbody>
            <?php foreach ($room as $item): ?>
                <tr>
                    <td>
                        <?php if (is_null($item['picture_url'])) : ?>
                        <?php else: ?>
                            <img height="200" width="300" src="<?= esc($item['picture_url']); ?>"
                                 alt="<?= esc($item['price']); ?>">
                        <?php endif ?>
                    </td>
                    <td><?= esc($item['room_number']); ?></td>
                    <td><?= esc($item['number_seats']); ?></td>
                    <td><?= esc($item['price']); ?></td>
                    <td><?= esc($item['email']); ?></td>
                    <td>
                        <a href="<?= base_url() ?>/room/view/<?= esc($item['id']); ?>"
                           class="btn btn-primary btn-sm">Просмотреть</a>
                        <a href="<?= base_url() ?>/room/edit/<?= esc($item['id']); ?>"
                           class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url() ?>/room/delete/<?= esc($item['id']); ?>"
                           class="btn btn-danger btn-sm">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="text-center">
            <p>Комнаты не созданы</p>
            <div class="d-flex justify-content-center mb-2">
                <?= form_open('room/viewAllWithUsers', ['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Номер комнаты или количество мест" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>
            <a class="btn btn-primary btn-lg" href="<?= base_url() ?>/room/create"><span
                        class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать комнату</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

