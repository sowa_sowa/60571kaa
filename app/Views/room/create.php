<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 600px; background-color: #F7F4DEED">

        <?= form_open_multipart('room/store'); ?>
        <div class="form-group">
            <label for="name">Номер комнаты</label>
            <input type="text" class="form-control <?= ($validation->hasError('room_number')) ? 'is-invalid' : ''; ?>"
                   name="room_number"
                   value="<?= old('room_number'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('room_number') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Количество мест</label>
            <input type="text" class="form-control <?= ($validation->hasError('number_seats')) ? 'is-invalid' : ''; ?>"
                   name="number_seats"
                   value="<?= old('number_seats'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('number_seats') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Стоимость</label>
            <input type="text" class="form-control <?= ($validation->hasError('price')) ? 'is-invalid' : ''; ?>"
                   name="price"
                   value="<?= old('price'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('price') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>
    </div>

<?= $this->endSection() ?>