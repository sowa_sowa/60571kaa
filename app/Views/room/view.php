<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main" >
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($room)) : ?>
        <div class="card md-4" style="max-width: 700px; background-color: #F7F4DEED">
            <div class="row">
                <div class="col-md-5 d-flex align-items-center">
                    <?php if (is_null($room['picture_url'])) : ?>
                    <?php else:?>
                        <img height="150" src="<?= esc($room['picture_url']); ?>"
                             class="card-img" alt="<?= esc($room['room_number']); ?>">
                    <?php endif ?></div>
                <div class="col-md-7">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="my-0">Номер комнаты:</div>
                            <div class="text-muted">№<?= esc($room['room_number']); ?></div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="my-0">Количетсво мест:</div>
                            <div class="text-muted"><?= esc($room['number_seats']); ?> человек</div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="my-0">Стоимость:</div>
                            <span><?= esc($room['price']); ?> рублей</span>
                        </div>
                    </div>
                    <?php if ($ionAuth->isAdmin()): ?>
                        <a href="<?= base_url()?>/index.php/room/edit/<?= esc($room['id']); ?>" class="btn btn-primary btn-sm">Редактировать</a>
                        <a href="<?= base_url()?>/index.php/room/delete/<?= esc($room['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                    <?php else : ?>
                        <a href="<?= base_url()?>/index.php/room" class="btn btn-primary btn-sm">Бронировать</a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

