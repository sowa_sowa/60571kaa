<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center" style="margin: 0; background-color: #F7F4DEED">
    <img class="mb-4" src="https://img-premium.flaticon.com/png/512/3780/3780959.png?token=exp=1621320315~hmac=76a9560402d9d102c525c2197f49be1e"
         alt="" width="72" height="72"><h1 class="display-4">Hotel</h1>
    <p class="lead">Это приложение поможет людям найти номер в гостинице</p>
    <?php if (!$ionAuth->loggedIn()): ?>
    <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

