<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public MyServer $server;

    public $dsn = 'pgsql:host=ec2-54-220-195-236.eu-west-1.compute.amazonaws.com;port=5432;dbname=ddimvfl8fm86ig;user=yowdugbgactjib;password=ba3d808a784a9d5833fe327c096f52be857d09e59316e11ac1fa293e28b3bb3e';

    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $config = array('access_lifetime' => 86400);
        $storage = new MyPdo(array('dsn' => $this->dsn), array('user_table' => 'users', 'username' => '', 'password' => ''));
        $grantType = new UserCredentials($storage);
        $this->server = new MyServer($storage, $config);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}