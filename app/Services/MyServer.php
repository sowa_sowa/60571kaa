<?php

namespace App\Services;

use OAuth2\Server;

class MyServer extends Server
{
    public function getTokenForFrontend(string $userID, string $clientID): array
    {
        $token = $this->createDefaultAccessTokenResponseType();
        return $token->createAccessToken($clientID, $userID);
    }

}