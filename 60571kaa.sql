-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 15 2021 г., 08:08
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571kaa`
--

-- --------------------------------------------------------

--
-- Структура таблицы `accommodation`
--

CREATE TABLE `accommodation` (
  `id` int NOT NULL COMMENT 'id проживания',
  `id_room` int DEFAULT NULL COMMENT 'id комнаты',
  `id_guest` int DEFAULT NULL COMMENT 'id гостя',
  `start_date` date NOT NULL COMMENT 'дата начала',
  `end_date` date NOT NULL COMMENT 'дата окончания',
  `number_people` int DEFAULT NULL COMMENT 'количество человек'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `accommodation`
--

INSERT INTO `accommodation` (`id`, `id_room`, `id_guest`, `start_date`, `end_date`, `number_people`) VALUES
(1, 8, 8, '2021-02-01', '2021-02-10', 2),
(2, 9, 9, '2021-02-09', '2021-02-23', 4),
(3, 10, 10, '2021-02-03', '2021-02-21', 3),
(4, 11, 11, '2021-02-06', '2021-02-16', 2),
(5, 12, 12, '2021-02-01', '2021-02-24', 1),
(6, 13, 13, '2021-02-10', '2021-02-18', 2),
(7, 14, 14, '2021-02-02', '2021-02-18', 1),
(8, 15, 15, '2021-02-04', '2021-02-16', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `guest`
--

CREATE TABLE `guest` (
  `id` int NOT NULL COMMENT 'id гостя',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ФИО гостя'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `guest`
--

INSERT INTO `guest` (`id`, `name`) VALUES
(1, 'Петров Иван Иванович'),
(2, 'Иванов Петр Петрович'),
(3, 'Джамалов Иса Султанович'),
(4, 'Некрасов Сергей Владимирович'),
(5, 'Котов Александр Васильевич'),
(6, 'Котова Вероника Алексеевна'),
(7, 'Бондарчук Ксения Игоревна'),
(8, 'Глазкова Наталья Николаевна'),
(9, 'Глазков Николай Петрович'),
(10, 'Бурков Илья Васильевич'),
(11, 'Лисный Артур Владимирович'),
(12, 'Правдин Владислав Игоревич'),
(13, 'Щупов Иван Алексеевич'),
(14, 'Круглов Виктор Станиславович'),
(15, 'Щербаков Петр Валерьевич');

-- --------------------------------------------------------

--
-- Структура таблицы `housing`
--

CREATE TABLE `housing` (
  `id` int NOT NULL COMMENT 'id корпуса',
  `name` varchar(255) NOT NULL COMMENT 'наименование корпуса'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `housing`
--

INSERT INTO `housing` (`id`, `name`) VALUES
(1, 'Снегирь'),
(2, 'Лондон');

-- --------------------------------------------------------

--
-- Структура таблицы `reservation`
--

CREATE TABLE `reservation` (
  `id` int NOT NULL COMMENT 'id бронирования',
  `id_room` int DEFAULT NULL COMMENT 'id комнаты',
  `id_guest` int DEFAULT NULL COMMENT 'id гостя',
  `start_date` date NOT NULL COMMENT 'дата начала',
  `end_date` date NOT NULL COMMENT 'дата окончания',
  `number_people` int DEFAULT NULL COMMENT 'количество человек'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reservation`
--

INSERT INTO `reservation` (`id`, `id_room`, `id_guest`, `start_date`, `end_date`, `number_people`) VALUES
(1, 1, 1, '2021-02-01', '2021-02-04', 2),
(2, 2, 2, '2021-01-01', '2021-02-01', 4),
(3, 3, 3, '2021-02-02', '2021-02-16', 3),
(4, 4, 4, '2021-02-01', '2021-02-04', 2),
(5, 5, 5, '2021-02-04', '2021-02-08', 1),
(6, 6, 6, '2021-02-03', '2021-02-16', 2),
(7, 7, 7, '2021-02-08', '2021-02-16', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `room`
--

CREATE TABLE `room` (
  `id` int NOT NULL COMMENT 'id комнаты',
  `id_housing` int DEFAULT NULL COMMENT 'id корпуса',
  `room_number` int DEFAULT NULL COMMENT 'номер комнаты',
  `number_seats` int DEFAULT NULL COMMENT 'количество мест',
  `price` int DEFAULT NULL COMMENT 'цена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `room`
--

INSERT INTO `room` (`id`, `id_housing`, `room_number`, `number_seats`, `price`) VALUES
(1, 1, 101, 2, 2500),
(2, 1, 111, 4, 5000),
(3, 1, 95, 3, 4000),
(4, 1, 90, 1, 1500),
(5, 1, 91, 1, 2500),
(6, 1, 92, 2, 4000),
(7, 1, 93, 2, 3500),
(8, 1, 94, 2, 2500),
(9, 1, 100, 4, 5000),
(10, 1, 102, 3, 6000),
(11, 2, 50, 2, 2500),
(12, 2, 51, 1, 3000),
(13, 2, 52, 2, 2000),
(14, 2, 53, 1, 3000),
(15, 2, 54, 3, 4500),
(16, 2, 55, 2, 4000),
(17, 2, 56, 3, 6000),
(18, 2, 57, 3, 10000),
(19, 2, 58, 2, 8000),
(20, 2, 59, 1, 3000);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `accommodation`
--
ALTER TABLE `accommodation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `id_room` (`id_room`);

--
-- Индексы таблицы `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `housing`
--
ALTER TABLE `housing`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_room` (`id_room`),
  ADD KEY `id_guest` (`id_guest`);

--
-- Индексы таблицы `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_housing` (`id_housing`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `accommodation`
--
ALTER TABLE `accommodation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id проживания', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `guest`
--
ALTER TABLE `guest`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id гостя', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `housing`
--
ALTER TABLE `housing`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id корпуса', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id бронирования', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `room`
--
ALTER TABLE `room`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id комнаты', AUTO_INCREMENT=21;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `accommodation`
--
ALTER TABLE `accommodation`
  ADD CONSTRAINT `accommodation_ibfk_1` FOREIGN KEY (`id_guest`) REFERENCES `guest` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `accommodation_ibfk_2` FOREIGN KEY (`id_room`) REFERENCES `room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`id_guest`) REFERENCES `guest` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`id_housing`) REFERENCES `housing` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
